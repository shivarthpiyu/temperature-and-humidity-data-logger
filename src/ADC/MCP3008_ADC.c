/** @file problem4.c
 *  @brief solution to problem 4
 *  
 *  Interfacing MCP3008 ADC using Shunya Interfaces API
 *  
 *  @author Shriram
 *  @NOTE compiled and checked for syntax errors with ShunyaOS Docker Container 
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* --- Project Includes --- */
#include <shunyaInterfaces.h>


/* byte data type */
typedef unsigned char byte;

/* Global Variables */
bool hwSPI;  
uint8_t sck;  // sck pin
uint8_t mosi; // mosi pin
uint8_t miso; // miso pin
uint8_t cs;   // cs pin
/* spi bus */
uint8_t bus;

/*
 *#####################################################################
 *  Process block
 *  -------------
 *  Solve all your problems here 
 *#####################################################################
 */

/*!
 *    @brief  read ADC value
 *    @param  channel
 *            channel number 
 *    @param  differential
 *            differential mode enabled or disabled
 *    @return data that was read 
 */
int SPIxADC(uint8_t channel, bool differential)
{
        /* variables to store command and differential mode preference */
        byte command, sgldiff;
        
        if (differential) {
                sgldiff = 0;
        }// end of if differential  
        else {
                sgldiff = 1;
        }// end of else 

        /* command to be transmitter over SPI Bus */
        command = ((0x01 << 7) |          // start bit
                (sgldiff << 6) |          // single or differential
                ((channel & 0x07) << 3)); // channel number

        if (hwSPI) { //if HW SPI is used 
                
                /* variables to store the bytes */
                byte b0, b1, b2;
                /* start the SPI transaction with BAUD rate, MSBFIRST, spi_mode0 */
                spiBeginTransaction(3600000, MSBFIRST, 0);
                /* pull cs pin to high */
                digitalWrite(cs, LOW);
                
                /* request the vaue from the slave device */
                b0 = spiTransfer(command);
                b1 = spiTransfer(0x00);
                b2 = spiTransfer(0x00);

                /* pull cs pin to high */
                digitalWrite(cs, HIGH);
                /* stop the SPI transaction */
                spiEndTransaction();
                 
                 /* return the required value read from MISO line */
                return 0x3FF & ((b0 & 0x01) << 9 | (b1 & 0xFF) << 1 | (b2 & 0x80) >> 7);

        }// end of HW SPI 
        else { //if SW SPI is used 

                /* variables to hold the input and output buffer */
                uint16_t outBuffer, inBuffer = 0;
                /* pull cs pin to low */
                digitalWrite(cs, LOW);

                // 5 command bits + 1 null bit + 10 data bits = 16 bits
                outBuffer = command << 8;
                for (int c = 0; c < 16; c++) {
                        
                        /* write to MOSI pin */
                        digitalWrite(mosi, (outBuffer >> (15 - c)) & 0x01);
                        /* pull sck pin to high */
                        digitalWrite(sck, HIGH);
                        /* pull sck pin to low */
                        digitalWrite(sck, LOW);
                        inBuffer <<= 1;

                        /* read from the miso line */
                        if (digitalRead(miso))
                        inBuffer |= 0x01;                
                }
                /* pull cs pin to high */
                digitalWrite(cs, HIGH);

                /* return the required value read from MISO line */
                return inBuffer & 0x3FF;
        }// end of SW SPI
}

/*!
 *    @brief  Initialize for hardware SPI
 *    @param  cs
 *            number of CSPIN (Chip Select)
 *    @param  theSPI
 *            optional SPI object
 *    @return true if process is successful
 */
bool hw_spi_init(uint8_t cs)
{
        /* set hwSPI to true since we are using hw_spi initialization*/
        hwSPI = true;
        /* set chip select pin to output mode */
        pinMode(cs, OUTPUT);
        /* pull cs pin to low */
        digitalWrite(cs, HIGH);
        /* start the SPI Communication */
        spiBegin(bus);

        return true;
}


/*!
 *    @brief  Initialize for software SPI
 *    @param  sck
 *            number of pin used for SCK (Serial Clock)
 *    @param  mosi
 *            number of pin used for MOSI (Master Out Slave In)
 *    @param  miso
 *            number of pin used for MISO (Master In Slave Out)
 *    @param  cs
 *            number of pin used for CS (Chip Select)
 *    @return true if process is successful
 */
bool sw_spi_init(uint8_t sck, uint8_t mosi, uint8_t miso, uint8_t cs) 
{
        /* set hwSPI to false since we are using sw_spi initialization*/
        hwSPI = false;

        /* set SCK pin to output */
        pinMode(sck, OUTPUT);
        /* set MOSI pin to output */ 
        pinMode(mosi, OUTPUT);
        /* set MISO pin to input */ 
        pinMode(miso, INPUT);
        /* set CS pin to output */ 
        pinMode(cs, OUTPUT);

        /* make sck pin low */
        digitalWrite(sck, LOW);
        /* make mosi pin low */
        digitalWrite(mosi, LOW);
        /* make chip select pin high */
        digitalWrite(cs, HIGH);

        return true;
}   

/*!
 *    @brief  Read single ended ADC channel.
 *    @param  channel number
 *            
 *    @return -1 if channel < 0 or channel > 7, otherwise ADC (int)
 */
int readADC(uint8_t channel)
{
        /* return -1 if the channel number is not between 0 & 7 */
        if ((channel < 0) || (channel > 7))
                return -1;
        
        /* call SPIxADC in Single ended mode */
        return SPIxADC(channel, false);
}  

/*!
 *    @brief  Read differential ADC channel.
 *    @param  differential
 *            0: Return channel 0 minus channel 1
 *            1: Return channel 1 minus channel 0
 *            2: Return channel 2 minus channel 3
 *            3: Return channel 3 minus channel 2
 *            4: Return channel 4 minus channel 5
 *            5: Return channel 5 minus channel 4
 *            6: Return channel 6 minus channel 7
 *            7: Return channel 7 minus channel 6
 *    @return -1 if channel < 0 or channel > 7, otherwise ADC difference (int)
 */
int readADCDifference(uint8_t differential)
{
        /* return -1 if the channel number is not between 0 & 7 */
        if ((differential < 0) || (differential > 7))
                return -1;
         
        /* call SPIxADC() fucntion in differential mode*/
        return SPIxADC(differential, true);
}


/*
 *#####################################################################
 *  main() funtion
 *  -------------
 *#####################################################################
 */
int main()
{       
        /* set appropriate GPIO pins for SPI */
        sck = 1;
        mosi = 2;
        miso = 3;
        cs = 4;

         /* call the initialization function with the pin numbers */
        sw_spi_init(sck, mosi, miso, cs);	
  
         /* read from all 8 channels and display the output */
        for (int chan=0; chan<=7; chan++) {
                /* print the values from adc */
                printf("%d",readADC(chan));
                printf("\n");
        
        }// end of for loop 

        return 0;

}// end of main()

